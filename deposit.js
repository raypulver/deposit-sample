'use strict';

const request = require('request-promise');
const Web3 = require('web3');
const BN = require('bignumber.js');
const RPC_URL = 'https://mainnet.infura.io/aR7WPNCrZhhnYRnn8yRT';
const web3 = new Web3(new Web3.providers.HttpProvider(RPC_URL));
const { promisify } = require('bluebird');
const { property, bindKey } = require('lodash');
const { unitMap } = require('web3-utils');
const abi = require('web3-eth-abi');
const Transaction = require('ethereumjs-tx');
const { bufferToHex } = require('ethereumjs-util');

const [
  getTransactionReceipt,
  getBlockNumber,
  estimateGas,
  getTransactionCount
] = [
  'getTransactionReceipt',
  'getBlockNumber',
  'estimateGas',
  'getTransactionCount'
].map((v) => promisify(bindKey(web3.eth, v)));

const wrapRPCCall = (fn) => (...args) => fn(...args).then(property('result'));

let id = 0;
const sendRawTransaction = wrapRPCCall((hex) => request({
  url: RPC_URL,
  method: 'POST',
  json: {
    method: 'eth_sendRawTransaction',
    id: id++,
    params: [ hex ]
  }
}));

const getGasPrice = () => request({
  url: 'https://ethgasstation.info/json/ethgasAPI.json',
  method: 'GET'
}).then(JSON.parse).then(({ fast }) => +new BN(fast).times(unitMap.gwei).dividedToIntegerBy(10).toPrecision());

const sendTx = ({
  to,
  data,
  wallet,
  value,
  nonce
}) => Promise.all([
  (nonce !== Number(nonce) ? getTransactionCount(wallet.getAddressString(), 'pending') : Promise.resolve(nonce)),
  estimateGas({
    to,
    data,
    from: wallet.getAddressString()
  }).catch(() => 150000),
  getGasPrice()
]).then(([ nonce, gas, gasPrice ]) => {
  const tx = new Transaction({
    from: wallet.getAddressString(),
    to,
    data,
    nonce,
    gas: Number(gas),
    value,
    gasPrice
  });
  tx.sign(wallet.getPrivateKey());
  return sendRawTransaction(bufferToHex(tx.serialize()));
});

const ETHER_ADDRESS = '0x0000000000000000000000000000000000000000';
const EXCHANGE_ADDRESS = '0x2a0c0dbecc7e4d658f48e01e3fa353f44050c208';
const AURA_ADDRESS = '0xcdcfc0f66c522fd086a1b725ea3c0eeb9f9e8814';
const AMOUNT = '0.1';

const PRECISION_MAP = {
  [ETHER_ADDRESS]: 18,
  [AURA_ADDRESS]: 18
};

const wallet = require('ethereumjs-wallet').fromV3(require('./wallet.json'), 'password-goes-here');

const adjustForPrecision = (token, amount) => new BN(amount).times(new BN(10).pow(PRECISION_MAP[token])).toPrecision();

const timeout = (n) => new Promise((resolve) => setTimeout(resolve, n));

const waitUntilTxMined = (tx) => Promise.all([
  getTransactionReceipt(tx),
  getBlockNumber()
]).then(([ receipt, number ]) => receipt && receipt.blockNumber >= number && receipt || timeout(2500).then(() => waitUntilTxMined(tx))).catch(() => timeout(2500).then(() => waitUntilTxMined(tx)));

const deposit = ({
  token,
  amount,
  wallet
}) => getTransactionCount(wallet.getAddressString(), 'pending').then((nonce) => token === ETHER_ADDRESS ? sendTx({
  wallet,
  nonce,
  to: EXCHANGE_ADDRESS,
  data: abi.encodeFunctionCall({
    name: 'deposit',
    inputs: []
  }, []),
  value: adjustForPrecision(token, amount)
}).then((tx) => {
  console.log('ether deposit for ' + amount + ' at ' + tx);
  return tx;
}) : sendTx({
  wallet,
  nonce,
  to: token,
  data: abi.encodeFunctionCall({
    name: 'approve',
    inputs: [{
      name: 'target',
      type: 'address'
    }, {
      name: 'amount',
      type: 'uint256'
    }]
  }, [ EXCHANGE_ADDRESS, adjustForPrecision(token, amount) ])
}).then((tx) => {
  console.log('deposit tx 1/2 submitted at ' + tx);
  return tx;
}).then(() => sendTx({
  wallet,
  nonce: Number(nonce) + 1,
  to: EXCHANGE_ADDRESS,
  data: abi.encodeFunctionCall({
    name: 'depositToken',
    inputs: [{
      name: 'token',
      type: 'address'
    }, {
      name: 'amount',
      type: 'uint256'
    }]
  }, [ token, adjustForPrecision(token, amount) ])
}).then((tx) => {
  console.log('deposit tx 2/2 available at ' + tx);
  return tx;
}))).then(waitUntilTxMined).then((receipt) => {
  console.log('transaction mined!');
  console.log('receipt:');
  console.log(require('util').inspect(receipt, { colors: true }));
});

deposit({
  token: AURA_ADDRESS,
  wallet,
  amount: AMOUNT
}).catch((err) => console.error(err.stack));
